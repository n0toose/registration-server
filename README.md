## Introduction

This is the registration server used for the join.codeberg.org form.

## Security

Incoming user data from forms is immediately encrypted using GnuPG Public-Key Encryption. No private key is ever stored on the server.

Encrypted registration records are sent via email to the registration accounts, from where they are fetched and processed off-line.


## Files, Overview

```
README.md                                ## This file
main.go                                  ## Source code of the reg-server 
etc/                                     ## The config file template folder, copied to target host at deployment
etc/systemd/system/reg-server.service    ## Systemd service to launch, monitor and restart the reg-service
etc/reg-server/public-key.asc.gpg        ## The public GnuPG key used for encryption on the server
templates/                               ## content templates
locales/                                 ## Translation related files. Translations welcome.
```

## Build+test locally

```shell
make
cd ./etc/reg-server
/tmp/build/gitea/bin/reg-server
```

Then point your browser at http://localhost:5000/

## Build+deploy

You may want to start `ssh-agent` and do `ssh-add` before calling `make deployment`, to avoid typing your ssh passphrase several times. Then do:

```shell
make -C HOSTNAME_FQDN=<hostname> deployment
```

## Translations

This tool uses gettext-compatible translation files. See the [go-gettext-module documentation](https://github.com/snapcore/go-gettext) for further details. 

Basically, it expects language files in locales/{de,en,...}/messages.mo.

As the .po files use for msgids message-stubs rather then the untranslated strings, you can use the English version as a base for your translation (so your poeditor can help as it should), thanks to the `poswap` utility. When you are done translating, you can swap the .po file back. If you already have a partially localized file, make sure to use it during the swapping. To install `poswap` locally, install translate-toolkit, for example like this: `sudo apt install translate-toolkit`. See more details about `poswap` [here](http://docs.translatehouse.org/projects/translate-toolkit/en/latest/commands/poswap.html) or [this StackOverflow](https://stackoverflow.com/a/5598209) discussion.

To use (for example) the English version as a base for your translation (example given for Czech language translation):

```shell
poswap locales/en/messages.po -t locales/cs/messages.po -o locales/cs/messages.po 
```

After you are done translating, poswap it back:

```shell
poswap --reverse locales/en/messages.po -t locales/cs/messages.po -o locales/cs/messages.po
```

The .mo files can be generated from the .po files using msgfmt:

```shell
find locales -name "*.po" -execdir msgfmt "{}" ";"
```

## Environment variables

The mail setup uses the following environment variables:

```shell
export MAIL_REGISTRATION_RECIPIENT=codeberg-test@codeberg.org
export MAIL_FROM=codeberg-test@codeberg.org
export MAIL_PASSWORD="THE-MAIL-PASSWORD"
export MAIL_HOST=mail.codeberg.local
export MAIL_PORT=587

/tmp/build/reg-server/bin/reg-server
```

To test the functionality you can set the environment variables to match your environment.

You must add your public key (matching MAIL_FROM) to etc/reg-server/public-key.asc.gpg if you want to be able to receive the encrypted mails.

## License

This software (excluding bundled fonts) is licensed under GNU AGPL. See the LICENSE file for details.

Logo material is by @mray and licensed under CC-BY. http://creativecommons.org/licenses/by/4.0/

Codeberg and the Codeberg Logo are trademarks of Codeberg e.V.
